import 'package:cloud_firestore/cloud_firestore.dart';

class SalonModel{
  late String name, address, docId;
  late DocumentReference reference;

  SalonModel({required this.name, required this.address, required this.docId,});

  SalonModel.fromJson(Map<String, dynamic> json) {
    address = json['address'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['address'] = this.address;
    data['name'] = this.name;
    return data;
  }
}