class ServiceModel {
  late String name, docId;
  late int price;

  ServiceModel({required this.name, required this.price});

  ServiceModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    price = json['price'] == null ? 0 : int.parse(json['price']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['price'] = this.price;
    return data;
  }
}