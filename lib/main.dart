import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth_ui/flutter_auth_ui.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mooi/fcm/fcm_background_handler.dart';
import 'package:mooi/fcm/fcm_notification_handler.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/ui/barber_booking_history.dart';
import 'package:mooi/ui/booking_screen.dart';
import 'package:mooi/ui/done_services_screen.dart';
import 'package:mooi/ui/home_screen.dart';
import 'package:mooi/ui/staff_home_screen.dart';
import 'package:mooi/ui/user_history_screen.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/main/main_view_model_imp.dart';
import 'package:page_transition/page_transition.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;
AndroidNotificationChannel? channel;


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // firebase
  await Firebase.initializeApp();

  //firebase setup
  FirebaseMessaging.onBackgroundMessage(firebaseBackgroundHandler);

  // Flutter local notifications
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  channel = const AndroidNotificationChannel('mooi.app', 'Mooi App', 'This is my channel', importance: Importance.max);

  await flutterLocalNotificationsPlugin!.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel!);

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(alert: true, badge: true, sound: true,);

  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'mooi',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/home':
            return PageTransition(child: HomePage(), type: PageTransitionType.fade, settings: settings);
          case '/booking':
            return PageTransition(child: BookingScreen(), type: PageTransitionType.fade, settings: settings);
          case '/history':
            return PageTransition(child: UserHistoryScreen(), type: PageTransitionType.fade, settings: settings);
          case '/staffHome':
            return PageTransition(child: StaffHomeScreen(), type: PageTransitionType.fade, settings: settings);
          case '/doneServices':
            return PageTransition(child: DoneServicesScreen(), type: PageTransitionType.fade, settings: settings);
          case '/barberHistory':
            return PageTransition(child: BarberHistoryScreen(), type: PageTransitionType.fade, settings: settings);
          default: return null;
        }
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'mooi first page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  final mainViewModel = MainViewModelImp();

  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 2),
    vsync: this,
  )..repeat(reverse: true);

  late final Animation<Offset> _offsetAnimation = Tween<Offset>(
    begin: Offset.zero,
    end: const Offset(0.03, 0.0),
  ).animate(CurvedAnimation(
    parent: _controller,
    curve: Curves.slowMiddle,
  ));

  @override
  void initState() {
    super.initState();

    // get token, subscribe here

    // get token
    FirebaseMessaging.instance.getToken()
    .then((value) => print('Token: $value'));

    // subscribe to topic, example is: demoSubscribe
    FirebaseMessaging.instance.subscribeToTopic(FirebaseAuth.instance.currentUser!.uid)
    .then((value) => print('Success...'));

    // setup message display
  initFirebaseMessagingHandler(channel!);


  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/my_bg.png'),
                  fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.all(16.0),
                width: MediaQuery.of(context).size.width,
                child: FutureBuilder(
                  future: mainViewModel.checkLoginState(context, false, _scaffoldKey),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      var userState = snapshot.data as LOGIN_STATE;
                      if (userState == LOGIN_STATE.LOGGED) {
                        return Container();
                      } else {
                        return SlideTransition(
                          position: _offsetAnimation,
                          child: ElevatedButton.icon(
                            icon: Icon(Icons.phone),
                            label: Text('LOGIN WITH PHONE'),
                            style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color(0xff373737))),

                            onPressed: () => mainViewModel.processLogin(context, _scaffoldKey),
                          ),
                        );
                      }
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
