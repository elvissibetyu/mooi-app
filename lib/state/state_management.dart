import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mooi/models/barber_model.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/models/service_model.dart';
import 'package:mooi/models/user_model.dart';

final userLogged = StateProvider((ref) => FirebaseAuth.instance.currentUser);
final userToken = StateProvider((ref) => '');
final forceReload = StateProvider((ref) => false);

final userInformation = StateProvider((ref) => UserModel(name: '', address: ''));

// booking state
final currentStep = StateProvider((ref) => 1);
final selectedCity = StateProvider((ref) => CityModel(name: '', address: ''));
final selectedSalon = StateProvider((ref) => SalonModel(name: '', address: '', docId: '',));
final selectedBarber = StateProvider((ref) => BarberModel(name: '', rating: 0.0, ratingTimes: 0, docId: ''));
final selectedDate = StateProvider((ref) => DateTime.now());
final selectedTimeSlot = StateProvider((ref) => -1);
final selectedTime = StateProvider((ref) => '');

final deleteFlatRefresh = StateProvider((ref) => false);

// Staff

final staffStep = StateProvider((ref) => 1);
final selectedBooking = StateProvider((ref) => BookingModel(
    barberId: '',
    barberName: '',
    cityBook: '',
    customerId: '',
    customerName: '',
    customerPhone: '',
    done: false,
    salonAddress: '',
    salonId: '',
    salonName: '',
    slot: -1,
    timeStamp: -1,
    time: '',
    docId: '',
    services: '',
    totalPrice: 0,
    reference:
    FirebaseFirestore.instance.collection('collectionPath').doc()));
final selectedServices = StateProvider((ref) => List<ServiceModel>.empty(growable: true));

// loading
final isLoading = StateProvider((ref) => false);
