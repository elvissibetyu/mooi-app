
import 'package:mooi/models/service_model.dart';
import 'package:ntp/ntp.dart';

enum LOGIN_STATE { LOGGED, NOT_LOGGED}

const TIME_SLOT = {
  '09:00-9:45',
  '10:00-10:45',
  '11:00-11:45',
  '12:00-12:45',
  '13:00-13-45',
  '14:00-14:00',
  '15:00-15:45',
  '16:00-16:45',
  '17:00-17:45',
  '18:00-18:45',
  '19:00-19:45'
};

Future<int> getMaxAvailableTimeSlots(DateTime dt) async {
  DateTime now = dt.toLocal();
  int offset = await NTP.getNtpOffset(localTime: now);
  DateTime syncTime = now.add(Duration(milliseconds: offset));
  // compare sync time with local time to enable time slot
  if (syncTime.isBefore(DateTime(now.year, now.month, now.day, 9, 0 ))) {
    return 1;
  } else
  if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 9, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 9, 45 ))) {
    return 1;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 10, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 10, 45 ))) {
    return 2;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 11, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 11, 45 ))) {
    return 3;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 12, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 12, 45 ))) {
    return 4;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 13, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 13, 45 ))) {
    return 5;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 14, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 14, 45 ))) {
    return 6;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 15, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 15, 45 ))) {
    return 7;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 16, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 16, 45 ))) {
    return 8;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 17, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 17, 45 ))) {
    return 9;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 18, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 18, 45 ))) {
    return 10;
  } else if (syncTime.isAfter(DateTime(now.year, now.month, now.day, 19, 0)) && syncTime.isBefore(DateTime(now.year, now.month, now.day, 19, 45 ))) {
    return 11;
  } else return 21;
}

Future<DateTime> syncTime() async {
  var now = DateTime.now();
  var offset = await NTP.getNtpOffset(localTime: now);
  return now.add(Duration(milliseconds: offset));
}

String convertServices(List<ServiceModel> services) {
  String result = '';
  if (services.length > 0 ){
    services.forEach((element) { 
      result += '${element.name},  ';
    });
  }
  return result.substring(0, result.length - 2);
}