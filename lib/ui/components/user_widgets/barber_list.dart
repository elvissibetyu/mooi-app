import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mooi/models/barber_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/view_model/booking/booking_view_model.dart';

displayBarber(BookingViewModel bookingViewModel,  SalonModel salon) {
  return FutureBuilder(
    future: bookingViewModel.displayBarberBySalon(salon),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        var barbers = snapshot.data as List<BarberModel>;
        if (barbers.length == 0) {
          return Center(
            child: Text(barberListEmptyText),
          );
        } else {
          return ListView.builder(
            key: PageStorageKey('keep'),
            itemCount: barbers.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () =>
                bookingViewModel.onSelectedBarber(context, barbers[index]),
                child: Card(
                  child: ListTile(
                    onTap: null,
                    leading: Icon(
                      Icons.person,
                      color: Color(0xffbcbcbc),
                    ),
                    trailing: bookingViewModel.isBarberSelected(context, barbers[index])
                        ? Icon(
                      Icons.check_circle_outline,
                      color: Color(0xffff87b7),
                    )
                        : null,
                    title: Text(
                      '${barbers[index].name}',
                      style:
                      GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                    ),
                    subtitle: RatingBar.builder(
                      ignoreGestures: true,
                      itemSize: 16,
                      allowHalfRating: true,
                      initialRating: barbers[index].rating,
                      direction: Axis.horizontal,
                      itemCount: 5,
                      itemPadding: EdgeInsets.all(4),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Color(0xffff87b7),
                      ),
                      onRatingUpdate: (newRating) {},
                    ),
                  ),
                ),
              );
            },
          );
        }
      }
    },
  );
}