import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/view_model/booking/booking_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';


displayCityList(BookingViewModel bookingViewModel) {
  return FutureBuilder(
    future: bookingViewModel.displayCities(),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        var cities = snapshot.data as List<CityModel>;
        if (cities.length == 0) {
          return Center(
            child: Text(cityListEmptyText),
          );
        } else {
          return ListView.builder(
            key: PageStorageKey('keep'),
            itemCount: cities.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => bookingViewModel.onSelectedCity(context, cities[index]),
                child: Card(
                  child: ListTile(
                    onTap: null,
                    leading: Icon(
                      Icons.home_work,
                      color: Color(0xffbcbcbc),
                    ),
                    trailing: bookingViewModel.isCitySelected(context, cities[index])
                        ? Icon(
                      Icons.check_circle_outline,
                      color: Color(0xffff87b7),
                    )
                        : null,
                    title: Text(
                      '${cities[index].name}',
                      style: GoogleFonts.comfortaa(
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              );
            },
          );
        }
      }
    },
  );
}