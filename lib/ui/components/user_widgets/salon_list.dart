import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/view_model/booking/booking_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

displaySalon(BookingViewModel bookingViewModel, String city) {
  return FutureBuilder(
    future: bookingViewModel.displaySalonByCity(city),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        var salons = snapshot.data as List<SalonModel>;
        if (salons.length == 0) {
          return Center(
            child: Text('There is no data here.'),
          );
        } else {
          return ListView.builder(
            key: PageStorageKey('keep'),
            itemCount: salons.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () =>
                context.read(selectedSalon).state = salons[index],
                child: Card(
                  child: ListTile(
                    onTap: null,
                    leading: Icon(
                      Icons.home_outlined,
                      color: Color(0xffbcbcbc),
                    ),
                    trailing: context.read(selectedSalon).state.docId ==
                        salons[index].docId
                        ? Icon(
                      Icons.check_circle_outline,
                      color: Color(0xffff87b7),
                    )
                        : null,
                    title: Text(
                      '${salons[index].name}',
                      style:
                      GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(
                      '${salons[index].address}',
                      style:
                      GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              );
            },
          );
        }
      }
    },
  );
}