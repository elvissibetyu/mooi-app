import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/view_model/booking/booking_view_model.dart';

displayConfirm(BookingViewModel bookingViewModel, BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Expanded(
        flex: 1,
        child: Padding(
          padding: EdgeInsets.all(24),
          child: Image.asset('assets/images/logo.png'),
        ),
      ),
      Expanded(
        flex: 3,
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  Text(
                    thankServiceText.toUpperCase(),
                    style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    bookingInformationText.toUpperCase(),
                    style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Icon(Icons.calendar_today),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '${context.read(selectedTime).state} - ${DateFormat('dd/MM/yyyy').format(context.read(selectedDate).state)}'
                            .toUpperCase(),
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Icon(Icons.person),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '${context.read(selectedBarber).state.name}'
                            .toUpperCase(),
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  Divider(
                    thickness: 2,
                  ),
                  Row(
                    children: [
                      Icon(Icons.home),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '${context.read(selectedSalon).state.name}'
                            .toUpperCase(),
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Icon(Icons.location_on),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        '${context.read(selectedSalon).state.address}'
                            .toUpperCase(),
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(height: 8,),
                  ElevatedButton(
                    onPressed: () => bookingViewModel.confirmBooking(context, scaffoldKey),
                    child: Text('Confirm'),
                    style: ButtonStyle(
                        backgroundColor:
                        MaterialStateProperty.all(Color(0xffff87b7))),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    ],
  );
}