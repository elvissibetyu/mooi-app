import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/staff_home/staff_home_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

displayAppointments(StaffHomeViewModel staffHomeViewModel, BuildContext context) {
  return FutureBuilder(
      future: staffHomeViewModel.isStaffOfThisSalon(context),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator(),);
        } else {
          var result = snapshot.data as bool;
          if(result) {
            return displaySlots(staffHomeViewModel, context);
          } else {
            return Center(child: Text('You don\'t belong wena'),);
          }
        }
      }
  );
}

displaySlots(StaffHomeViewModel staffHomeViewModel,  BuildContext context) {
  var now = context.read(selectedDate).state;
  return Column(
    children: [
      Container(
        color:  Color(0xffff87b7),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Center(
                child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    children: [
                      Text('${DateFormat.MMMM().format(now)}', style: GoogleFonts.comfortaa(color: Colors.white, fontWeight: FontWeight.bold),),
                      Text('${now.day}', style: GoogleFonts.comfortaa(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 24),),
                      Text('${DateFormat.EEEE().format(now)}', style: GoogleFonts.comfortaa(color: Colors.white, fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    minTime: DateTime.now(),
                    maxTime: now.add(Duration(days: 62)),
                    onConfirm: (date) => context.read(selectedDate).state = date
                );
              },
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.calendar_today, color: Colors.white,),
                ),
              ),
            ),
          ],
        ),
      ),
      Expanded(
        child: FutureBuilder(
          future: staffHomeViewModel.displayMaxAvailableTimeSlot(context.read(selectedDate).state),
          builder: (context, snapshot) {
            var maxAvailableSlot = snapshot.data as int;
            return FutureBuilder(
              future: staffHomeViewModel.displayBookingSlotOfBarber(context, DateFormat('dd_MMMM_yyyy').format(context.read(selectedDate).state)),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  var takenTimeSlots = snapshot.data as List<int>;
                  return GridView.builder(
                    key: PageStorageKey('keep'),
                    itemCount: TIME_SLOT.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                    ), itemBuilder: (context, index) => GestureDetector(
                    onTap:  !takenTimeSlots.contains(index) ? null : () => staffHomeViewModel.processDoneServices(context, index),
                    child: Card(
                      color: staffHomeViewModel.getColorOfThisSlot(context, takenTimeSlots, index, maxAvailableSlot),
                      child: GridTile(
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('${TIME_SLOT.elementAt(index)}', style: GoogleFonts.comfortaa(fontSize: 18),),
                              Text(takenTimeSlots.contains(index) ? takenText : maxAvailableSlot > index ? notAvailableText :  availableText, style: GoogleFonts.comfortaa(),)
                            ],
                          ),
                        ),
                        header: context.read(selectedTime).state == TIME_SLOT.elementAt(index) ? Icon(Icons.check_circle_outline, color: Color(0xffff87b7),) : null,
                      ),
                    ),
                  ),
                  );
                }
              },
            );
          },
        ),
      ),
    ],
  );
}