import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/view_model/staff_home/staff_home_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

staffDisplaySalon(StaffHomeViewModel staffHomeViewModel,  String name) {
  return FutureBuilder(
    future: staffHomeViewModel.displaySalonByCity(name),
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        var salons = snapshot.data as List<SalonModel>;
        if (salons.length == 0) {
          return Center(
            child: Text(staffCannotLoadSalon),
          );
        } else {
          return
            ListView.builder(
              itemCount: salons.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => staffHomeViewModel.onSelectedSalon(context, salons[index]),
                  child: Card(
                    child: ListTile(
                      shape: staffHomeViewModel.isSalonSelected(context, salons[index]) ?
                      RoundedRectangleBorder(side: BorderSide(color: Colors.green, width: 4), borderRadius: BorderRadius.circular(5))
                          : null,
                      onTap: null,
                      leading: Icon(Icons.home_outlined, color: Color(0xffbcbcbc),),
                      trailing: context.read(selectedSalon).state.docId == salons[index].docId ? Icon(Icons.check_circle_outline, color: Color(0xffff87b7),) : null,
                      title: Text('${salons[index].name}', style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),),
                      subtitle: Text('${salons[index].address}', style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold),),
                    ),
                  ),
                );
              },

            );
        }
      }
    },
  );
}