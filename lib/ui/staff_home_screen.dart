import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/all_salon_ref.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/string/strings.dart';
import 'package:mooi/ui/components/staff_widgets/appointment_list.dart';
import 'package:mooi/ui/components/staff_widgets/city_list.dart';
import 'package:mooi/ui/components/staff_widgets/salon_list.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/staff_home/staff_home_view_model_imp.dart';

class StaffHomeScreen extends ConsumerWidget {
  final staffHomeViewModel = StaffHomeViewModelImp();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var currentStaffStep = watch(staffStep).state;
    var cityWatch = watch(selectedCity).state;
    var salonWatch = watch(selectedSalon).state;
    var dateWatch = watch(selectedDate).state;

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Color(0xfffdf9ee),
        appBar: AppBar(
          title: Text(
            currentStaffStep == 1
                ? selectCityText
                : currentStaffStep == 2
                    ? selectSalonText
                    : currentStaffStep == 3
                        ? myAppointmentsText
                        : staffHomeText,
            style: GoogleFonts.comfortaa(color: Colors.black),
          ),
          backgroundColor: Color(0xffbcbcbc),
          actions: [
            currentStaffStep == 3 ?
                InkWell(
                  child: Icon(Icons.history),
                  onTap: () => Navigator.of(context).pushNamed('/barberHistory'),
                ) : Container()
          ],
        ),
        body: Column(
          children: [
            // Area
            Expanded(
              flex: 10,
              child: currentStaffStep == 1
                  ? staffDisplayCity(staffHomeViewModel)
                  : currentStaffStep == 2
                      ? staffDisplaySalon(staffHomeViewModel, cityWatch.name)
                      : currentStaffStep == 3
                          ? displayAppointments(staffHomeViewModel, context)
                          : Container(),
            ),

            // Buttons
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xffFF87B7))),
                          child: Text(
                            previousText,
                            style: GoogleFonts.comfortaa(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: currentStaffStep == 1
                              ? null
                              : () => context.read(staffStep).state--,
                        ),
                      ),
                      SizedBox(
                        width: 55,
                      ),
                      Expanded(
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Color(0xffFF87B7))),
                          child: Text(
                            nextText,
                            style: GoogleFonts.comfortaa(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          onPressed: (currentStaffStep == 1 &&
                                      context
                                              .read(selectedCity)
                                              .state
                                              .name
                                              .length ==
                                          0) ||
                                  (currentStaffStep == 2 &&
                                      context
                                              .read(selectedSalon)
                                              .state
                                              .docId
                                              .length ==
                                          0) ||
                                  currentStaffStep == 3
                              ? null
                              : () => context.read(staffStep).state++,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
