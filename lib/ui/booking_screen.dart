import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:im_stepper/stepper.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/all_salon_ref.dart';
import 'package:mooi/models/barber_model.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/ui/components/user_widgets/barber_list.dart';
import 'package:mooi/ui/components/user_widgets/city_list.dart';
import 'package:mooi/ui/components/user_widgets/confirm_booking.dart';
import 'package:mooi/ui/components/user_widgets/salon_list.dart';
import 'package:mooi/ui/components/user_widgets/time_slot.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/booking/booking_view_model_imp.dart';
import 'package:mooi/widgets/my_loading_widget.dart';

class BookingScreen extends ConsumerWidget {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  final bookingViewModel = new BookingViewModelImp();

  final TextEditingController searchController = TextEditingController();
  List<CityModel> searchResult = [];

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var step = watch(currentStep).state;
    var cityWatch = watch(selectedCity).state;
    var salonWatch = watch(selectedSalon).state;
    var barberWatch = watch(selectedBarber).state;
    var dateWatch = watch(selectedDate).state;
    var timeWatch = watch(selectedTime).state;
    var timeSlotWatch = watch(selectedTimeSlot).state;

    var isLoadingWatch = watch(isLoading).state;

    return SafeArea(
        child: Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text(
          'Booking',
          style: GoogleFonts.comfortaa(color: Colors.black),
        ),
        backgroundColor: Color(0xffbcbcbc),
        leading: IconButton(
            onPressed: () => Navigator.of(context).pop(),
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
      ),
      backgroundColor: Color(0xfffdf9ee),
      body: Column(
        children: [
          // stepper
          NumberStepper(
            activeStep: step - 1,
            direction: Axis.horizontal,
            enableNextPreviousButtons: false,
            enableStepTapping: false,
            numbers: [1, 2, 3, 4, 5],
            stepColor: Color(0xffbcbcbc),
            activeStepColor: Color(0xffFF87B7),
            numberStyle: GoogleFonts.comfortaa(color: Colors.black),
            lineColor: Color(0xffFF87B7),
            stepReachedAnimationEffect: Curves.easeInOutQuint,
            stepReachedAnimationDuration: Duration(seconds: 1500),
            steppingEnabled: true,
          ),
          // screen
          step == 1
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        'Select your area',
                        style: GoogleFonts.comfortaa(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ],
                  ),
                )
              : step == 2
                  ? Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                              child: Text(
                            'Who do you want to book an appointment with?',
                            style: GoogleFonts.comfortaa(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                        ],
                      ),
                    )
                  : Container(),
          step == 1
              ? Container(
                  margin: EdgeInsets.only(right: 50, left: 10),
                  child: TextField(
                    onChanged: onSearchTextChanged,
                    cursorColor: Color(0xffff87b7),
                    autofocus: false,
                    controller: searchController,
                    decoration: InputDecoration(
                        focusColor: Color(0xffff87b7),
                        fillColor: Color(0xffff87b7),
                        hoverColor: Color(0xffff87b7),
                        hintText: 'or search your area',
                        icon: Icon(Icons.search, color: Color(0xffff87b7)),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffff87b7)),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffff87b7)),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffff87b7)),
                        ),
                        hintStyle: GoogleFonts.comfortaa(fontSize: 16)),
                  ),
                )
              : step == 2
                  ? Container(
                      margin: EdgeInsets.only(right: 50, left: 10),
                      child: TextField(
                        onChanged: onSearchTextChanged,
                        cursorColor: Color(0xffff87b7),
                        autofocus: false,
                        controller: searchController,
                        decoration: InputDecoration(
                            focusColor: Color(0xffff87b7),
                            fillColor: Color(0xffff87b7),
                            hoverColor: Color(0xffff87b7),
                            hintText: 'search the person',
                            icon: Icon(Icons.search, color: Color(0xffff87b7)),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffff87b7)),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffff87b7)),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffff87b7)),
                            ),
                            hintStyle: GoogleFonts.comfortaa(fontSize: 16)),
                      ),
                    )
                  : Container(),
          SizedBox(
            height: 10,
          ),
          Expanded(
            flex: 10,
            child: step == 1
                ? displayCityList(bookingViewModel)
                : step == 2
                    ? displaySalon(bookingViewModel, cityWatch.name)
                    : step == 3
                        ? displayBarber(bookingViewModel, salonWatch)
                        : step == 4
                            ? displayTimeSlot(bookingViewModel, context, barberWatch)
                            : step == 5
                                ? isLoadingWatch ? MyLoadingWidget(text: 'We are setting up your booking.') : displayConfirm(bookingViewModel, context, scaffoldKey)
                                : Container(),
          ),
          // Button
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Color(0xffFF87B7))),
                        child: Text(
                          'Previous',
                          style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        onPressed: step == 1
                            ? null
                            : () => context.read(currentStep).state--,
                      ),
                    ),
                    SizedBox(
                      width: 55,
                    ),
                    Expanded(
                      child: ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Color(0xffFF87B7))),
                        child: Text(
                          'Next',
                          style: GoogleFonts.comfortaa(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                        onPressed: (step == 1 &&
                                    context.read(selectedCity).state.name.length ==
                                        0) ||
                                (step == 2 &&
                                    context
                                            .read(selectedSalon)
                                            .state
                                            .docId.length == 0) ||
                                (step == 3 &&
                                    context.read(selectedBarber).state.docId.length == 0) ||
                                (step == 4 &&
                                    context.read(selectedTimeSlot).state ==
                                        -1) ||
                                step == 5
                            ? null
                            : () => context.read(currentStep).state++,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    ));
  }

  onSearchTextChanged(String text) async {
    searchResult.clear();
    if (text.isEmpty) {
      // setState(() {});
      return;
    }

    var cities = await getCities();

    cities.forEach((city) {
      if (city.name.contains(text)) searchResult.add(city);
    });

    // setState(() {});
  }
}
