import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/user_ref.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/user_history/user_history_view_model_imp.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class UserHistoryScreen extends ConsumerWidget {

  final scaffoldKey = GlobalKey<ScaffoldState>();
 final userHistoryViewModel = UserHistoryViewModelImp();

  final TextEditingController searchController = TextEditingController();
  final List<CityModel> searchResult = [];

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var watchRefresh = watch(deleteFlatRefresh);
    return SafeArea(
        child: Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomInset: true,
          appBar: AppBar(

            title: Text('User History', style: GoogleFonts.comfortaa(color: Colors.black),),
            backgroundColor: Color(0xffbcbcbc),
            leading: IconButton(onPressed: () => Navigator.of(context).pop(), icon: Icon(Icons.arrow_back, color: Colors.black,)),
          ),
          backgroundColor: Color(0xfffdf9ee),
          body: Padding(
            padding: EdgeInsets.all(12.0),
            child: FutureBuilder(
              future: userHistoryViewModel.displayUserHistory(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else {
                  var userBookings = snapshot.data as List<BookingModel>;
                  if (userBookings.length == 0) {
                    return Center(
                      child: Text('no bookings'),
                    );
                  } else {
                    return FutureBuilder(
                        future: syncTime(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState == ConnectionState.waiting) {
                            return Center(child: CircularProgressIndicator(),);
                          } else {
                            var syncTime = snapshot.data as DateTime;
                            return ListView.builder(
                              itemCount: userBookings.length,
                              itemBuilder: (context, index) {
                                var isExpired = DateTime.fromMillisecondsSinceEpoch(userBookings[index].timeStamp).isBefore(syncTime);
                                return Card(
                                  elevation: 8,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(22)
                                    ),
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(padding: EdgeInsets.all(12),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Column(
                                                  children: [
                                                    Text('Date', style: GoogleFonts.comfortaa(),),
                                                    Text(DateFormat('dd/MM/yyyy').format(
                                                        DateTime.fromMillisecondsSinceEpoch(userBookings[index].timeStamp)
                                                    ), style: GoogleFonts.comfortaa(fontSize: 22, fontWeight: FontWeight.bold),)
                                                  ],
                                                ),
                                                Column(
                                                  children: [
                                                    Text('Time', style: GoogleFonts.comfortaa(),),
                                                    Text(TIME_SLOT.elementAt(userBookings[index].slot)
                                                      , style: GoogleFonts.comfortaa(fontSize: 22, fontWeight: FontWeight.bold),)
                                                  ],
                                                ),
                                              ],
                                            ),
                                            Divider(thickness: 1, indent: 10, endIndent: 10,),
                                            Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(userBookings[index].salonName, style: GoogleFonts.comfortaa(fontSize: 20, fontWeight: FontWeight.bold),),
                                                SizedBox(height: 8,),
                                                Text(userBookings[index].barberName, style: GoogleFonts.comfortaa(),),
                                                SizedBox(height: 8,),
                                                Text(userBookings[index].salonAddress, style: GoogleFonts.comfortaa(),)
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: (userBookings[index].done ||  isExpired) ? null : (){
                                          Alert(
                                              context: context,
                                              type: AlertType.warning,
                                              title: 'DELETE BOOKING',
                                              desc: 'Remember to also delete the booking in your calender',
                                              buttons: [
                                                DialogButton(child: Text('CANCEL', style: TextStyle(color: Colors.black),), onPressed: () => Navigator.of(context).pop(), color: Colors.white,),
                                                DialogButton(child: Text('DELETE', style: TextStyle(color: Colors.white),), onPressed: () {
                                                  Navigator.of(context).pop();
                                                  userHistoryViewModel.userCancelBooking(context, userBookings[index]);
                                                })
                                              ]
                                          ).show();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.blue,
                                              borderRadius: BorderRadius.only(
                                                  bottomLeft: Radius.circular(22),
                                                  bottomRight: Radius.circular(22)
                                              )
                                          ),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Padding(
                                                padding: EdgeInsets.symmetric(vertical: 10),
                                                child: Text(
                                                  userBookings[index].done ? 'COMPLETE' :
                                                  isExpired ? 'EXPIRED' :
                                                  'CANCEL', style:  GoogleFonts.comfortaa(color: isExpired ? Colors.grey :  Colors.white, fontWeight: FontWeight.bold),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                );
                              },

                            );
                          }
                        }
                    );
                  }
                }
              },
            ),
          )
        ));
  }
}



