import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mooi/cloud_firestore/banner_ref.dart';
import 'package:mooi/cloud_firestore/lookbook_ref.dart';
import 'package:mooi/cloud_firestore/user_ref.dart';
import 'package:mooi/models/image_model.dart';
import 'package:mooi/models/user_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/view_model/home/home_view_model.dart';
import 'package:mooi/view_model/home/home_view_model_imp.dart';

class HomePage extends ConsumerWidget {

  final homeViewModel = HomeViewModelImp();


  @override
  Widget build(BuildContext context, ScopedReader watch) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Color(0xfffdf9ee),
        body: SingleChildScrollView(
          child: Column(
            children: [
              FutureBuilder(
                future: homeViewModel.displayUserProfile(context,
                    FirebaseAuth.instance.currentUser?.phoneNumber ?? ''),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    var userModel = snapshot.data as UserModel;
                    return Container(
                      decoration: BoxDecoration(color: Color(0xffbcbcbc)),
                      padding: EdgeInsets.all(14),
                      child: Row(
                        children: [
                          CircleAvatar(
                            child: Icon(
                              Icons.person,
                              color: Color(0xffbcbcbc),
                              size: 22,
                            ),
                            backgroundColor: Color(0xffFF87B7),
                            maxRadius: 22,
                          ),
                          SizedBox(width: 20,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('${userModel.name}', style: GoogleFonts.comfortaa(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold),),
                                Text('${userModel.address}', overflow: TextOverflow.ellipsis, style: GoogleFonts.comfortaa(fontSize: 14, color: Colors.black, fontWeight: FontWeight.bold),)
                              ],
                            ),
                          ),
                          homeViewModel.isStaff(context) ?
                              IconButton(onPressed: () => Navigator.of(context).pushNamed('/staffHome'),
                                icon: Icon(Icons.admin_panel_settings)
                              ) : Container()
                        ],
                      ),
                    );
                  }
                },
              ),
              // menu
              Padding(
                padding: EdgeInsets.all(4),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () => Navigator.pushNamed(context, '/booking'),
                        child: Container(
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.book_online, size: 50, color: Color(0xffbcbcbc)),
                                  Text('Booking', style: GoogleFonts.comfortaa(),)
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ),
                    Expanded(
                        child: Container(
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(8),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.shopping_cart, size: 50, color: Color(0xffbcbcbc)),
                                  Text('Cart', style: GoogleFonts.comfortaa(),)
                                ],
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: GestureDetector(
                          onTap: () => Navigator.of(context).pushNamed('/history'),
                          child: Container(
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(8),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(Icons.history, size: 50, color: Color(0xffbcbcbc)),
                                    Text('History', style: GoogleFonts.comfortaa(),)
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              ),
              // Banner
              FutureBuilder(
                future: homeViewModel.displayBanner(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(child: CircularProgressIndicator(),);
                  } else {
                    print('${snapshot.data}');
                    var banners = snapshot.data as List<ImageModel>;
                    return CarouselSlider(
                      items: banners.map((e) => Container(child: ClipRRect(borderRadius: BorderRadius.all(Radius.circular(8)), child: Image.network(e.image),),)).toList(),
                      options: CarouselOptions(
                        enlargeCenterPage: true,
                        aspectRatio: 3.0,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3)
                      )
                    );
                  }
                }
              ),
              // look book
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Text('LOOKBOOK', style: GoogleFonts.comfortaa(fontWeight: FontWeight.bold, fontSize: 24, color: Color(0xff373737)),),
                  ],
                ),
              ),
              FutureBuilder(
                  future: homeViewModel.displayLookBook(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Center(child: CircularProgressIndicator(),);
                    } else {
                      var lookbook = snapshot.data as List<ImageModel>;
                      return Column(
                         children:
                           lookbook.map((e) => Container(
                             padding: EdgeInsets.all(8),
                             child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Image.network(e.image)),
                           )).toList()
                         ,
                      );
                    }
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
