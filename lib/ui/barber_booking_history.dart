import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/state/staff_user_history_state.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/barber_booking_history/barber_booking_history_view_model_imp.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


class BarberHistoryScreen extends ConsumerWidget {

  final barberBookingHistoryViewModel = BarberBookingHistoryViewModelImpl();

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    var dateWatch = watch(barberHistorySelectedDate).state;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Barber History', style: GoogleFonts.comfortaa(color: Colors.black),),
          backgroundColor: Color(0xffbcbcbc),
          leading: IconButton(onPressed: () => Navigator.of(context).pop(), icon: Icon(Icons.arrow_back, color: Colors.black,)),
        ),
        body: Column(
          children: [
            Container(
              color: Color(0xffff87b7),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(12),
                        child: Column(
                          children: [
                            Text(
                              '${DateFormat.MMMM().format(dateWatch)}',
                              style: GoogleFonts.comfortaa(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${dateWatch.day}',
                              style: GoogleFonts.comfortaa(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 24),
                            ),
                            Text(
                              '${DateFormat.EEEE().format(dateWatch)}',
                              style: GoogleFonts.comfortaa(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          onConfirm: (date) =>
                          context.read(barberHistorySelectedDate).state = date);
                    },
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Icon(
                          Icons.calendar_today,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: FutureBuilder(
                future: barberBookingHistoryViewModel.getBarberBookingHistory(context, dateWatch),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    var userBookings = snapshot.data as List<BookingModel>;
                    if (userBookings.length == 0) {
                      return Center(
                        child: Text('You have no bookings this date'),
                      );
                    } else {
                      return FutureBuilder(
                          future: syncTime(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState == ConnectionState.waiting) {
                              return Center(child: CircularProgressIndicator(),);
                            } else {
                              var syncTime = snapshot.data as DateTime;
                              return ListView.builder(
                                itemCount: userBookings.length,
                                itemBuilder: (context, index) {
                                  return Card(
                                    elevation: 8,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(22)
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        Padding(padding: EdgeInsets.all(12),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  Column(
                                                    children: [
                                                      Text('Date', style: GoogleFonts.comfortaa(),),
                                                      Text(DateFormat('dd/MM/yyyy').format(
                                                          DateTime.fromMillisecondsSinceEpoch(userBookings[index].timeStamp)
                                                      ), style: GoogleFonts.comfortaa(fontSize: 22, fontWeight: FontWeight.bold),)
                                                    ],
                                                  ),
                                                  Column(
                                                    children: [
                                                      Text('Time', style: GoogleFonts.comfortaa(),),
                                                      Text(TIME_SLOT.elementAt(userBookings[index].slot)
                                                        , style: GoogleFonts.comfortaa(fontSize: 22, fontWeight: FontWeight.bold),)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              Divider(thickness: 1, indent: 10, endIndent: 10,),
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(userBookings[index].salonName, style: GoogleFonts.comfortaa(fontSize: 20, fontWeight: FontWeight.bold),),
                                                  SizedBox(height: 8,),
                                                  Text(userBookings[index].barberName, style: GoogleFonts.comfortaa(),),
                                                  SizedBox(height: 8,),
                                                  Text(userBookings[index].salonAddress, style: GoogleFonts.comfortaa(),)
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                },

                              );
                            }
                          }
                      );
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

}

