import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:mooi/models/service_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mooi/state/state_management.dart';

Future<List<ServiceModel>> getServices(BuildContext context) async {
  var services = List<ServiceModel>.empty(growable: true);
  CollectionReference serviceRef = FirebaseFirestore.instance.collection('Services');
  QuerySnapshot snapshot = await serviceRef.where(context.read(selectedSalon).state.docId).get();
  snapshot.docs.forEach((element) {
    var ele = element.data() as Map<String, dynamic>;
    var serviceModel = ServiceModel(name: '', price: 0);
    serviceModel.docId = element.id;
    serviceModel.name = ele['name'];
    serviceModel.price = ele['price'];
    services.add(serviceModel);
  });
  return services;
}