import 'package:flutter/material.dart';
import 'package:mooi/models/booking_model.dart';

abstract class BarberBookingHistoryViewModel {
  Future<List<BookingModel>> getBarberBookingHistory(BuildContext context, DateTime dateTime);
}