import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/user_ref.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/view_model/user_history/user_history_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class UserHistoryViewModelImp implements UserHistoryViewModel {
  @override
  Future<List<BookingModel>> displayUserHistory() {
    return getUserHistory();
  }

  @override
  void userCancelBooking(BuildContext context, BookingModel bookingModel) {
    var batch = FirebaseFirestore.instance.batch();
    var barberBooking = FirebaseFirestore.instance
        .collection('AllSalon').doc(bookingModel.cityBook)
        .collection('Branch').doc(bookingModel.salonId)
        .collection('Barber').doc(bookingModel.barberId)
        .collection(DateFormat('dd_MMMM_yyyy').format(DateTime.fromMillisecondsSinceEpoch(bookingModel.timeStamp)))
        .doc(bookingModel.slot.toString());

    var userBooking = bookingModel.reference;

    batch.delete(userBooking);
    batch.delete(barberBooking);

    batch.commit().then((value) {
      context.read(deleteFlatRefresh).state = !context.read(deleteFlatRefresh).state;
    });
  }

}