import 'package:flutter/material.dart';
import 'package:mooi/models/booking_model.dart';

abstract class UserHistoryViewModel {
  Future<List<BookingModel>> displayUserHistory();
  void userCancelBooking(BuildContext context, BookingModel bookingModel);
}