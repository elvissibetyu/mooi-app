import 'package:flutter/cupertino.dart';
import 'package:mooi/models/user_model.dart';
import 'package:mooi/models/image_model.dart';

abstract class HomeViewModel {
  Future<UserModel> displayUserProfile(BuildContext context, String phoneNumber);
  Future<List<ImageModel>> displayBanner();
  Future<List<ImageModel>> displayLookBook();

  bool isStaff(BuildContext context);
}