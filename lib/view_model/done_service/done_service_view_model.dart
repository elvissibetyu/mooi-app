import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/service_model.dart';

abstract class DoneServicesViewModel {
  Future<BookingModel> displayDetailsBooking(BuildContext context, int timeSlot);
  Future<List<ServiceModel>> displayServices(BuildContext context);
  void finishService(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey);
  double calculateTotalPrice(List<ServiceModel> serviceSelected);
  bool isDone(BuildContext context);
  bool isSelectedService(BuildContext context, ServiceModel serviceModel);
  void onSelectedChip(BuildContext context, bool isSelected, ServiceModel e);
}