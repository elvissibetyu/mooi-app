import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/material/scaffold.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/all_salon_ref.dart';
import 'package:mooi/cloud_firestore/services_ref.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/service_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/done_service/done_service_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class DoneServiceViewModelImp implements DoneServicesViewModel {
  @override
  double calculateTotalPrice(List<ServiceModel> serviceSelected) {
    return serviceSelected.map((e) => e.price)
        .fold<double>(0, (value, element) =>(value + element));
  }

  @override
  Future<BookingModel> displayDetailsBooking(BuildContext context, int timeSlot) {
    return getBookingDetails(context, timeSlot);
  }

  @override
  Future<List<ServiceModel>> displayServices(BuildContext context) {
    return getServices(context);
  }

  @override
  void finishService(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) {
    var batch = FirebaseFirestore.instance.batch();
    var barberBook = context.read(selectedBooking).state;

    var userBook = FirebaseFirestore.instance.collection('User')
        .doc('${barberBook.customerPhone}').collection('Booking_${barberBook.customerId}')
        .doc('${barberBook.barberId}_${DateFormat('dd_MMMM_yyyy').format(DateTime.fromMillisecondsSinceEpoch(barberBook.timeStamp))}');

    Map<String, dynamic> updateDone = new Map();
    updateDone['done'] = true;
    updateDone['services'] = convertServices(context.read(selectedServices).state);
    updateDone['totalPrice'] = context.read(selectedServices).state.map((e) => e.price)
        .fold<double>(0, (previousValue, element) => previousValue + element);

    batch.update(userBook, updateDone);
    batch.update(barberBook.reference, updateDone);

    batch.commit().then((value) {
      ScaffoldMessenger.of(scaffoldKey.currentContext ?? context).showSnackBar(
          SnackBar(content: Text('Process Success'))
      ).closed.then((value) => Navigator.of(context).pop());
    });
  }

  @override
  bool isDone(BuildContext context) {
    // TODO: implement isDone
    throw UnimplementedError();
  }

  @override
  bool isSelectedService(BuildContext context, ServiceModel serviceModel) {
    // TODO: implement isSelectedService
    throw UnimplementedError();
  }

  @override
  void onSelectedChip(BuildContext context, bool isSelected, ServiceModel e) {
    // TODO: implement onSelectedChip
  }
}