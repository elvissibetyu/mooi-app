import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:mooi/cloud_firestore/all_salon_ref.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/staff_home/staff_home_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class StaffHomeViewModelImp implements StaffHomeViewModel {

  Future<List<CityModel>> displayCities() {
    return getCities();
  }

  @override
  bool isCitySelected(BuildContext context, CityModel cityModel) {
    return context.read(selectedCity).state.name == cityModel.name;
  }

  @override
  void onSelectedCity(BuildContext context, CityModel cityModel) {
    context.read(selectedCity).state = cityModel;
  }

  @override
  Future<List<SalonModel>> displaySalonByCity(String cityName) {
    return getSalonByCity(cityName);
  }

  @override
  bool isSalonSelected(BuildContext context, SalonModel salonModel) {
    return context.read(selectedSalon).state.docId == salonModel.docId;
  }

  @override
  void onSelectedSalon(BuildContext context, SalonModel salonModelModel) {
    context.read(selectedSalon).state = salonModelModel;
  }

  @override
  Future<List<int>> displayBookingSlotOfBarber(BuildContext context, String date) {
    return getBookingSlotOfBarber(context, date);
  }

  @override
  Future<int> displayMaxAvailableTimeSlot(DateTime dt) {
    return getMaxAvailableTimeSlots(dt);
  }

  @override
  Future<bool> isStaffOfThisSalon(BuildContext context) {
    return checkStaffOfThisSalon(context);
  }

  @override
  bool isTimeSlotBooked(List<int> listTimeSlot, int index) {
    return listTimeSlot.contains(index) ? false : true;
  }

  @override
  void processDoneServices(BuildContext context, int index) {
    context.read(selectedTimeSlot).state = index;
    Navigator.of(context).pushNamed('/doneServices');
  }

  @override
  Color? getColorOfThisSlot(BuildContext context, List<int> takenTimeSlots, int index, int maxAvailableSlot) {
    return takenTimeSlots.contains(index) ?
    Colors.grey[400]
        : maxAvailableSlot > index
        ? Colors.grey[400] :
    context.read(selectedTime).state == TIME_SLOT.elementAt(index) ?
    Colors.pink[100] :
    Colors.white;
  }


}