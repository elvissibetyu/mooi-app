import 'package:flutter/cupertino.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';

abstract class StaffHomeViewModel {

  // city
    Future<List<CityModel>> displayCities();
    void onSelectedCity(BuildContext context, CityModel cityModel);
    bool isCitySelected(BuildContext context, CityModel cityModel);

    // salon
    Future<List<SalonModel>> displaySalonByCity(String cityName);
    void onSelectedSalon(BuildContext context, SalonModel salonModelModel);
    bool isSalonSelected(BuildContext context, SalonModel salonModel);

    // appointment
    Future<bool> isStaffOfThisSalon(BuildContext context);
    Future<int> displayMaxAvailableTimeSlot(DateTime dt);
    Future<List<int>> displayBookingSlotOfBarber(BuildContext context, String date);
    bool isTimeSlotBooked(List<int> listTimeSlot, int index);
    void processDoneServices(BuildContext context, int index);
    Color? getColorOfThisSlot(BuildContext context, List<int> takenTimeSlots, int index, int maxAvailableSlot);
}