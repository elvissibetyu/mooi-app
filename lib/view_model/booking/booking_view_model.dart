
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mooi/models/barber_model.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/salon_model.dart';

abstract class BookingViewModel {
  //City
  Future<List<CityModel>> displayCities();
  void onSelectedCity(BuildContext context, CityModel cityModel);
  bool isCitySelected(BuildContext context, CityModel cityModel);

  // salon
  Future<List<SalonModel>> displaySalonByCity(String cityName);
  void onSelectedSalon(BuildContext context, SalonModel salonModelModel);
  bool isSalonSelected(BuildContext context, SalonModel salonModel);

  // barber
  Future<List<BarberModel>> displayBarberBySalon(SalonModel salonModel);
  void onSelectedBarber(BuildContext context, BarberModel barberModelModel);
  bool isBarberSelected(BuildContext context, BarberModel barberModel);

  // timeslot
  Future<int> displayMaxAvailableTimeslot(DateTime dt);
  Future<List<int>> displayTimeSlotOfBarber(BarberModel barberModel, String date);
  bool isAvailableForTapTimeSlot(int maxTime, int index, List<int> listTimeSlot);
  void onSelectedTimeSlot(BuildContext context, int index);
  Color? displayColorTimeSlot(BuildContext context, List<int> listTimeSlot, int index, int maxTimeSlot);

  void confirmBooking(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey);

  String appointmentTitle(BuildContext context, String name);

}