import 'dart:ui';

import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:intl/intl.dart';
import 'package:mooi/cloud_firestore/all_salon_ref.dart';
import 'package:mooi/fcm/notification_send.dart';
import 'package:mooi/models/barber_model.dart';
import 'package:mooi/models/booking_model.dart';
import 'package:mooi/models/city_model.dart';
import 'package:mooi/models/notification_payload_model.dart';
import 'package:mooi/models/salon_model.dart';
import 'package:mooi/state/state_management.dart';
import 'package:mooi/utils/utils.dart';
import 'package:mooi/view_model/booking/booking_view_model.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BookingViewModelImp implements BookingViewModel{
  @override
  Future<List<CityModel>> displayCities() {
    return getCities();
  }

  @override
  bool isCitySelected(BuildContext context, CityModel cityModel) {
    return context.read(selectedCity).state.name == cityModel.name;
  }

  @override
  void onSelectedCity(BuildContext context, CityModel cityModel) {
    context.read(selectedCity).state = cityModel;
  }

  @override
  Future<List<SalonModel>> displaySalonByCity(String cityName) {
    return getSalonByCity(cityName);
  }

  @override
  bool isSalonSelected(BuildContext context, SalonModel salonModel) {
    return context.read(selectedSalon).state.docId == salonModel.docId;
  }

  @override
  void onSelectedSalon(BuildContext context, SalonModel salonModelModel) {
    context.read(selectedSalon).state = salonModelModel;
  }

  @override
  Future<List<BarberModel>> displayBarberBySalon(SalonModel salonModel) {
    return getBarberBySalon(salonModel);
  }

  @override
  bool isBarberSelected(BuildContext context, BarberModel barberModel) {
    return context.read(selectedBarber).state.docId == barberModel.docId;
  }

  @override
  void onSelectedBarber(BuildContext context, BarberModel barberModelModel) {
    context.read(selectedBarber).state = barberModelModel;
  }

  @override
  Color? displayColorTimeSlot(BuildContext context, List<int> listTimeSlot, int index, int maxTimeSlot) {
    return
        listTimeSlot.contains(index)
        ? Colors.grey[400]
        : maxTimeSlot > index
        ? Colors.grey[400]
        : context.read(selectedTime).state ==
        TIME_SLOT.elementAt(index)
        ? Colors.pink[100]
        : Colors.white;
  }

  @override
  Future<int> displayMaxAvailableTimeslot(DateTime dt) {
    return getMaxAvailableTimeSlots(dt);
  }

  @override
  Future<List<int>> displayTimeSlotOfBarber(BarberModel barberModel, String date) {
    return getBarberTimeSlot(barberModel, date);
  }

  @override
  bool isAvailableForTapTimeSlot(int maxTime, int index, List<int> listTimeSlot) {
    return (maxTime > index) || listTimeSlot.contains(index);
  }

  @override
  void onSelectedTimeSlot(BuildContext context, int index) {
    context.read(selectedTimeSlot).state = index;
    context.read(selectedTime).state = TIME_SLOT.elementAt(index);
  }

  @override
  void confirmBooking(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) {
    var timeStamp = DateTime(
      context.read(selectedDate).state.year,
      context.read(selectedDate).state.month,
      context.read(selectedDate).state.day,
      int.parse(context.read(selectedTime).state.split(':')[0].substring(0, 2)),
      int.parse(context.read(selectedTime).state.split(':')[1].substring(0, 2)),
    ).millisecondsSinceEpoch;

    var bookingModel = new BookingModel(
        barberId: context.read(selectedBarber).state.docId,
        barberName: context.read(selectedBarber).state.name,
        cityBook: context.read(selectedCity).state.name,
        customerId: FirebaseAuth.instance.currentUser?.uid ?? '',
        customerName: context.read(userInformation).state.name,
        customerPhone: FirebaseAuth.instance.currentUser?.phoneNumber ?? '',
        done: false,
        salonAddress: context.read(selectedSalon).state.address,
        salonId: context.read(selectedSalon).state.docId,
        salonName: context.read(selectedSalon).state.name,
        slot: context.read(selectedTimeSlot).state,
        timeStamp: timeStamp,
        services: '',
        totalPrice: 0,
        time:
        '${context.read(selectedTime).state} - ${DateFormat('dd/MM/yyyy').format(context.read(selectedDate).state)}',
        docId: '',
        reference:
        FirebaseFirestore.instance.collection('collectionPath').doc());

    var batch = FirebaseFirestore.instance.batch();

    DocumentReference barberBooking = context
        .read(selectedBarber)
        .state
        .reference
        .collection(
        '${DateFormat('dd_MMMM_yyyy').format(context.read(selectedDate).state)}')
        .doc(context.read(selectedTimeSlot).state.toString());

    DocumentReference userBooking = FirebaseFirestore.instance
        .collection('User')
        .doc(FirebaseAuth.instance.currentUser?.phoneNumber)
        .collection('Booking_${FirebaseAuth.instance.currentUser?.uid}')
        .doc('${context.read(selectedBarber).state.docId}_${DateFormat('dd_MMMM_yyyy')
        .format(context.read(selectedDate).state)}');

    // set for batch
    batch.set(barberBooking, bookingModel.toJson());
    batch.set(userBooking, bookingModel.toJson());
    batch.commit().then((value) {

      context.read(isLoading).state = true;
      var notificationPayload = NotificationPayloadModel(to: '/topics/${context.read(selectedBarber).state.docId}',
          notification: NotificationContent(
            title: 'New booking',
            body: 'You have a new booking from ${FirebaseAuth.instance.currentUser!.phoneNumber}'
          ));

      sendNotification(notificationPayload)
      .then((value) {
        print(value);
        context.read(isLoading).state = false;

        Navigator.of(context).pop();
        ScaffoldMessenger.of(scaffoldKey.currentContext ?? context)
            .showSnackBar(SnackBar(
          content: Text('Your booking was successful.'),
        ));

        // reset values
        context.read(selectedDate).state = DateTime.now();
        context.read(selectedBarber).state =
            BarberModel(name: '', rating: 0.0, ratingTimes: 0, docId: '');
        context.read(selectedCity).state = CityModel(name: '', address: '');
        context.read(selectedSalon).state =
            SalonModel(name: '', address: '', docId: '');
        context.read(currentStep).state = 1;
        context.read(selectedTime).state = '';
        context.read(selectedTimeSlot).state = -1;
        // create event
        final event = Event(
            title: appointmentTitle(context, context.read(selectedBarber).state.name),
            description:
            'Appointment at ${context.read(selectedSalon).state.name} \n ${context.read(selectedSalon).state.address}'
                '\n ${context.read(selectedTime).state} - ${DateFormat('dd_MMMM_yyyy').format(context.read(selectedDate).state)}',
            location:
            '${context.read(selectedSalon).state.name}, ${context.read(selectedSalon).state.address}',
            startDate: DateTime(
                context.read(selectedDate).state.year,
                context.read(selectedDate).state.month,
                context.read(selectedDate).state.day,
                context.read(selectedDate).state.hour,
                context.read(selectedDate).state.minute),
            endDate: DateTime(
                context.read(selectedDate).state.year,
                context.read(selectedDate).state.month,
                context.read(selectedDate).state.day,
                context.read(selectedDate).state.hour,
                context.read(selectedDate).state.minute + 45),
            iosParams: IOSParams(reminder: Duration(minutes: 45)),
            androidParams: AndroidParams(emailInvites: []));

        Add2Calendar.addEvent2Cal(event).then((value) {});
      });

    });
  }

  @override
  String appointmentTitle(BuildContext context, String name) {
    return 'Appointment with $name';
  }

}